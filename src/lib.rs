use axum::{
    extract::{MatchedPath, Path},
    http::{HeaderValue, Method, Request, StatusCode},
    response::IntoResponse,
    routing::{any, get, post},
    Router,
};
use tower_http::{cors::CorsLayer, trace::TraceLayer};
use tracing::info_span;

use std::net::SocketAddr;

pub async fn serve() {
    let health = Router::new().route("/health", get(health_check));

    let inspector_routes = Router::new()
        .route("/", post(create_inspector))
        .route("/:id", get(get_inspector))
        .layer(
            CorsLayer::new()
                .allow_origin("http://localhost:3000".parse::<HeaderValue>().unwrap())
                .allow_methods([Method::GET]),
        );

    let app = Router::new()
        .merge(health)
        .nest("/inspector", inspector_routes)
        .route("/spy/:id", any(spy_handler))
        .layer(
            TraceLayer::new_for_http().make_span_with(|request: &Request<_>| {
                let matched_path = request
                    .extensions()
                    .get::<MatchedPath>()
                    .map(MatchedPath::as_str);

                info_span!("http_request", method = ?request.method(), matched_path)
            }),
        );

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    tracing::debug!("Starting on http://{}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn health_check() -> StatusCode {
    StatusCode::OK
}

async fn create_inspector() -> impl IntoResponse {
    (StatusCode::OK, "getting you a new inspector")
}

async fn get_inspector(Path(inspector_id): Path<String>) -> impl IntoResponse {
    (StatusCode::OK, inspector_id)
}

async fn spy_handler(Path(inspector_id): Path<String>) -> impl IntoResponse {
    (StatusCode::OK, inspector_id)
}
